<?php

namespace Tapbuy\PspPluginHipay\Plugin;

/**
 *
 * Plugin to extend Hipay plugin
 *
 * @author jerome@tapbuy.io
 *
 */

use Magento\Framework\App\RequestInterface;

class HipayPlugin{
    
    /**
     * after interceptor for method mapRequest() to be able to overwrite the Hipay order request
     * it's use mainly to change Hipay redirect URL to to Tpabuy URL
     * @param \HiPay\FullserviceMagento\Model\Request\Order $subject
     * @param \HiPay\Fullservice\Gateway\Request\Order\OrderRequest $result
     * @return \HiPay\Fullservice\Gateway\Request\Order\OrderRequest
     */
    public function afterMapRequest(\HiPay\FullserviceMagento\Model\Request\Order $subject, $result){
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        // check Tapbuy is enabled
        $tapbuyHelper = $objectManager->create('\Tapbuy\Checkout\Helper\Data');
        $isEnabled = $tapbuyHelper->getConfig('tapbuy_checkout/general/enabled');
        
        if($isEnabled){
        
            $request = $objectManager->create('Magento\Framework\Webapi\Request');
        
            // check if the Tapbuy header is present
            // we have to cnnage the redirect URLs only in Tapbuy context
            if($request->getHeader('X-Tapbuy-Call')){
            
                $body = json_decode($request->getContent());
            
                if(is_object($body)){
                
                    if(isset($body->paymentMethod->additional_data->accept_url)){
                        $result->accept_url = $body->paymentMethod->additional_data->accept_url."&order_ext_id=".$result->orderid;
                    }
                
                    if(isset($body->paymentMethod->additional_data->pending_url)){
                        $result->pending_url = $body->paymentMethod->additional_data->pending_url."&order_ext_id=".$result->orderid;
                    }
                
                    if(isset($body->paymentMethod->additional_data->decline_url)){
                        $result->decline_url = $body->paymentMethod->additional_data->decline_url."&order_ext_id=".$result->orderid;
                    }
                
                    if(isset($body->paymentMethod->additional_data->cancel_url)){
                        $result->cancel_url = $body->paymentMethod->additional_data->cancel_url."&order_ext_id=".$result->orderid;
                    }
                
                    if(isset($body->paymentMethod->additional_data->exception_url)){
                        $result->exception_url = $body->paymentMethod->additional_data->exception_url."&order_ext_id=".$result->orderid;
                    }
                
                    if(isset($body->paymentMethod->additional_data->notify_url)){
                        $result->notify_url = $body->paymentMethod->additional_data->notify_url;
                    }
                
                    if(isset($body->paymentMethod->additional_data->ipaddr)){
                        $result->ipaddr = $body->paymentMethod->additional_data->ipaddr;
                    }
                
                    // to record card token via HiPay server to server call, you have to set payment_type to OneClick
                    if(isset($body->paymentMethod->additional_data->payment_type) && !is_null($body->paymentMethod->additional_data->payment_type)){
                        $result->payment_type = $body->paymentMethod->additional_data->payment_type;
                    } elseif(isset($body->paymentMethod->additional_data->remember_card) && $body->paymentMethod->additional_data->remember_card){
                        $result->payment_type = 'OneClick';
                    }
                
                }
            
            }
        
        }
        
        return $result;
    }
    
}