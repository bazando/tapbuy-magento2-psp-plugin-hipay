<?php

namespace Tapbuy\PspPluginHipay\Plugin;

use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Quote\Api\Data\AddressInterface;

/**
 * 
 * plugin to handle payment API call for Tapbuy_Checkout when using Hipay
 * 
 * @author jerome@tapbuy.io
 *
 */

class TapbuyPaymentInformationManagement
{

    public function beforeSavePaymentInformationAndPlaceOrder(\Tapbuy\Checkout\Model\PaymentInformationManagement $subject, $cartId, PaymentInterface $paymentMethod, AddressInterface $billingAddress = null)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        $paymentAdditionaldata = $paymentMethod->getAdditionalData();
                
        // if the payment is using card alias we may have to get the card token from the DB
        // for a payment with an alias, the field card_token may contains the card_id from the table hipay_customer_card and NOT the token of the card
        // basicly if the card_token is numeric it's a card_id
        if(isset($paymentAdditionaldata['use_alias']) && $paymentAdditionaldata['use_alias']){
            
            if(is_numeric($paymentAdditionaldata['card_token'])){
                // get the card alias from DB and update the additionalData object
            
                // 1- get cart data from cart_id
                $cartInterceptor = $objectManager->create('Magento\Quote\Model\QuoteRepository\Interceptor');
                $cart = $cartInterceptor->get($cartId);
            
                if($cart){
                
                    // 2- get customer_id from cart
                    $customeId = $cart->getCustomerId();
                
                    // 3- get alias with alias_id and customer_id
                    $cardFactory = $objectManager->create('HiPay\FullserviceMagento\Model\CardFactory');
                
                    $card = $cardFactory
                    ->create()
                    ->getCollection()
                    ->addFieldToFilter('card_id', $paymentAdditionaldata['card_token'])
                    ->addFieldToFilter('customer_id', $customeId)
                    ->getFirstItem();
                
                    if( !empty($card->getCcToken())){
                        // 4- change the card token
                    
                        $paymentAdditionaldata['card_token'] = $card->getCcToken();
                        $paymentMethod->setAdditionalData($paymentAdditionaldata);
                    
                    } else{
                        // alias not found
                        throw new \Exception("Hipay Alias not found");
                    
                    }
                
                }
            
            } else{
                // it's an alphanumeric value so we consider it's already the token so nothing to do here
            }
            
        }
        
        return null;
        
    }
    
}