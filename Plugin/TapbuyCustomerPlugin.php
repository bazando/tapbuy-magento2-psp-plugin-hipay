<?php

namespace Tapbuy\PspPluginHipay\Plugin;

use Tapbuy\Checkout\Api\Data\TapbuyCardAlias;

/**
 * 
 * plugin to handle customer API call for Tapbuy_Checkout when using Hipay
 * 
 * @author jerome@tapbuy.io
 *
 */

class TapbuyCustomerPlugin 
{
    private function checkIfPSPRequested(string $psp){
        return $psp === "hipay";
    }

    /**
     * after plugin method to get the list of Hipay card aliases for a customer
     * 
     * @param \Tapbuy\Checkout\Model\Customer $subject
     * @param mixed $result
     * @param int $customerId
     * @param string $psp
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCardAlias[]
     */
    public function afterGetCardsAliases(\Tapbuy\Checkout\Model\Customer $subject, $result, $customerId, $psp)
    {
        if($this->checkIfPSPRequested($psp)){

            $result = [];
            $result = $this->getCardsAliases($customerId);
            return $result;
        }
        return $result;
    }
    
    /**
     * after plugin method to add tan Hipay card aliases for a customer
     * 
     * @param \Tapbuy\Checkout\Model\Customer $subject
     * @param mixed $result
     * @param int $customerId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCardAlias[]
     */
    public function afterAddCardAlias(\Tapbuy\Checkout\Model\Customer $subject, $result, $customerId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            
        $request = $objectManager->create('Magento\Framework\Webapi\Request');
        $body = json_decode($request->getContent());
            
        $cardFactory = $objectManager->create('\HiPay\FullserviceMagento\Model\CardFactory');
            
        foreach ($body as $cardData){
                
            // check if the token is not alreay saved into DB
            $card = $cardFactory->create();
            $card->getResource()->load($card, $cardData->card_token, 'cc_token');
            $cardTokenExist = (bool)$card->getId();
                
            if(!$cardTokenExist){
                // token not in DB so we add a new one
                    
                /** @var $card \HiPay\FullserviceMagento\Model\Card */
                $card = $cardFactory->create();
                    
                $card->setCcToken($cardData->card_token);
                $card->setCustomerId($customerId);
                $card->setCcExpMonth($cardData->card_exp_month);
                $card->setCcExpYear($cardData->card_exp_year);
                $card->setCcNumberEnc($cardData->card_encoded_number);
                $card->setCcType($cardData->card_type);
                $card->setCcOwner($cardData->card_name);
                $card->setCcStatus(\HiPay\FullserviceMagento\Model\Card::STATUS_ENABLED);
                $card->setName($cardData->alias_name);
                $card->setCreatedAt(new \DateTime());
                    
                $card->getResource()->save($card);
                    
            }
            
        }
        
        $result = [];
        $result = $this->getCardsAliases($customerId);
        return $result;
    }
    
    /**
     * after plugin method to delete tan Hipay card aliases for a customer
     * 
     * @param \Tapbuy\Checkout\Model\Customer $subject
     * @param mixed $result
     * @param int $customerId
     * @param string $cardAliasId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCardAlias[]
     */
    public function afterDeleteCardAlias(\Tapbuy\Checkout\Model\Customer $subject, $result, $customerId, $cardAliasId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        /** @var $card \HiPay\FullserviceMagento\Model\Card */
        $cardFactory = $objectManager->create('\HiPay\FullserviceMagento\Model\CardFactory');
        $card = $cardFactory->create();
        
        if(is_numeric($cardAliasId)){
            $card->getResource()->load($card, $cardAliasId, 'card_id');
        } else{
            $card->getResource()->load($card, $cardAliasId, 'cc_token');
        }
        
        $cardExist = (bool)$card->getId();
        
        if($cardExist && ($card->getCustomerId() == (string)$customerId)){
            // card alias exists and belongs to customer so we can delete it
            $card->getResource()->delete($card);
        }
        
        $result = [];
        $result = $this->getCardsAliases($customerId);
        return $result;
    }
    
    /**
     * get the list of card Alias stored into DB for a customer
     * @param int $customerId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCardAlias[]
     */
    protected function getCardsAliases($customerId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        // get card alias for customer
        $cardCollectionFactory = $objectManager->create('\HiPay\FullserviceMagento\Model\ResourceModel\Card\CollectionFactory');
        $cardCollection = $cardCollectionFactory->create();
        
        $cardCollection
        ->filterByCustomerId($customerId)
        ->addOrder('card_id', 'desc')
        ->onlyValid();
        
        $cardList = [];
        
        /** @var $card \HiPay\FullserviceMagento\Model\Card */
        foreach ($cardCollection as $card){
            
            $last4digit = substr($card->getCcNumberEnc(), -4);
            
            $alias = new TapbuyCardAlias();
            $alias
            ->setAliasId($card->getId())
            ->setCustomerId($card->getCustomerId())
            ->setAliasName($card->getName())
            ->setCardType($card->getCcType())
            ->setCardExpMonth($card->getCcExpMonth())
            ->setCardExpYear($card->getCcExpYear())
            ->setCardName($card->getCcOwner())
            ->setCardLast4Digit($last4digit)
            ->setCardEncodedNumber($card->getCcNumberEnc())
            ->setCardToken($card->getCcToken());
            
            $cardList[] = $alias;
            
        }
        
        return $cardList;
    }
    
}